import './style.css';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

const scene = new THREE.Scene();
scene.add(new THREE.AxesHelper(5));

//Light individuale
//const light = new THREE.SpotLight()
//light.position.set(5, 5, 5)
//scene.add(light)

const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 100000000)
camera.position.z = -50 //150//-50 //100
camera.position.y = 10 //10//10 //50
camera.position.x = -100 //0//-100 //0

const renderer = new THREE.WebGLRenderer()
renderer.setSize(window.innerWidth, window.innerHeight)
document.body.appendChild(renderer.domElement)

const controls = new OrbitControls(camera, renderer.domElement)
controls.enableDamping = true

const light = new THREE.AmbientLight(0x404040, 5); // soft white light
light.position.set
scene.add( light );

var pianeti = [];
var corpo, texture_corpo, material_corpo, sphere_corpo;

function corpo_celeste_sistema_solare(x, y, z, /*rotazione,*/ dimensione, texture, orbita){
    corpo = new THREE.SphereGeometry( dimensione, 100, 100 );
    texture_corpo = new THREE.TextureLoader().load(
        texture
    );
    material_corpo = new THREE.MeshBasicMaterial( {map: texture_corpo} );
    sphere_corpo = new THREE.Mesh( corpo, material_corpo );
    sphere_corpo.position.set(x, y, z);
    //sphere_corpo.rotation.set(rotazione, 0, 0);
    scene.add(sphere_corpo);
    pianeti.push(sphere_corpo);
    if(orbita == true) {	
        orbita_pianeti(Math.sqrt(x*x + y*y));
        //console.log("x_pianeti: " + x);
        //console.log("y_pianeti: " + y);
        //console.log("R: " + Math.sqrt(x*x + y*y));
    }
}

function saturno_anelli(x, y, z, rotazione, texture, orbita){
    const saturnoRing = new THREE.RingGeometry( 10, 15, 100 );
    const textureSaturnoRing = new THREE.TextureLoader().load(
        texture
    );
    const materialSaturnoRing = new THREE.MeshBasicMaterial( {map: textureSaturnoRing, side: THREE.DoubleSide} );
    const sphereSaturnoAnelli = new THREE.Mesh( saturnoRing, materialSaturnoRing );
    sphereSaturnoAnelli.position.set(x, y, z);
    sphereSaturnoAnelli.rotation.set(rotazione, 0, 0);
    scene.add(sphereSaturnoAnelli);
    pianeti.push(sphereSaturnoAnelli);
    if(orbita == true) {
        orbita_pianeti(Math.sqrt(x*x + y*y));
    }
}

function orbita_pianeti(r) {
    var segmenti = 100;
    var raggio = r;
    const geometria = new THREE.BufferGeometry();
    const array = new Array();
    var materiale = new THREE.LineBasicMaterial({color: 0x4d4d4d});
    for (var i = 0; i <= segmenti; i++) {
        var theta = (i / segmenti) * Math.PI * 2;
        //console.log("Theta: " + theta);
        const x = Math.cos(theta) * raggio;
        //console.log("X: " + x);
        const y = Math.sin(theta) * raggio;
        //console.log("Y: " + y);
        const z = 0;
        array.push(new THREE.Vector3(x, y, z));       
    }
    geometria.setFromPoints(array);
    var orbita_linea = new THREE.Line(geometria, materiale)
    orbita_linea.rotation.set(0, 0, 0); //x = -80
    scene.add(orbita_linea);
}

function rivoluzione_pianeti(m, massa){
    var raggio_rivoluzione = Math.sqrt(m.position.x * m.position.x + m.position.y * m.position.y);
    if(raggio_rivoluzione > 0){
        var pos = Math.atan(m.position.y / m.position.x);
        if(m.position.x < 0 && m.position.y >= 0) pos += Math.PI;
        if(m.position.x < 0 && m.position.y < 0) pos += Math.PI;
        if(m.position.x >= 0 && m.position.y < 0) pos += 2 * Math.PI;
        pos += Math.sqrt(massa * 0.0001 / raggio_rivoluzione);
        m.position.x = raggio_rivoluzione * Math.cos(pos);
        m.position.y = raggio_rivoluzione * Math.sin(pos);
    }
}

function anima_pianeti(attiva){
    if(attiva == true){
        for(var i = 0; i < pianeti.length; i++){
            pianeti[i].rotation.y += 0.01; //Rotazione
            rivoluzione_pianeti(pianeti[i], 1000); //Rivoluzione
        }
    }
}

//Adding Corpi
function adding_corpi(){
    corpo_celeste_sistema_solare(0, 0, 0, 55, './texture/sole.jpg', false);
    corpo_celeste_sistema_solare(100, 0, 0, 3, './texture/mercurio.jpg', true);
    corpo_celeste_sistema_solare(200, 0, 0, 6, './texture/venere.jpg', true);
    corpo_celeste_sistema_solare(300, 0, 0, 6, './texture/terra.jpg', true);
    corpo_celeste_sistema_solare(400, 0, 0, 4.5, './texture/marte.jpg', true);
    corpo_celeste_sistema_solare(500, 0, 0, 13, './texture/giove.jpg', true);
    saturno_anelli(600, 0, 0, 45, './texture/saturno_anelli.png', true);
    corpo_celeste_sistema_solare(600, 0, 0, 6.5, './texture/saturno.jpg', true);
    corpo_celeste_sistema_solare(700, 0, 0, 6.5, './texture/urano.jpg', true);
    corpo_celeste_sistema_solare(800, 0, 0, 6.5, './texture/nettuno.jpg', true);
}
adding_corpi();

function stelle(){
    var max = 60 * 80;
    var min = 30 * 80;
    var i = 0;
    while(i < 800){
        i += 1;
        var randomX = Math.floor((Math.random() * max)) - min;
        //console.log("X: " + randomX);
        var randomY = Math.floor((Math.random() * max)) - min;
        //console.log("Y: " + randomY);
        var randomZ = Math.floor((Math.random() * max)) - min;
        //console.log("Z: " + randomZ);
    
        const stella = new THREE.SphereGeometry( 2, 100, 100 ); //0.7
        const materialStella = new THREE.MeshBasicMaterial( {color: 0xffffff } );
        const sphereStella = new THREE.Mesh( stella, materialStella );
        sphereStella.position.set(randomX, randomY, randomZ);
        scene.add(sphereStella);
    }
}
//stelle();

window.addEventListener('resize', onWindowResize, false)
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight
    camera.updateProjectionMatrix()
    renderer.setSize(window.innerWidth, window.innerHeight)
    render()
}

function animate_camera(){
    camera.position.x += 0.1;
    camera.position.z += 0.5;
    console.log("X: " + camera.position.x);
    console.log("Z: " + camera.position.z);
}

function animate() {
    requestAnimationFrame(animate);
    controls.update();
    //Animazioni
    //animate_camera();
    anima_pianeti(true)
    render();
}

function render() {
    renderer.render(scene, camera);
}

animate();